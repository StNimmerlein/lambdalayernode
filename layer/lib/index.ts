const HOLY_FOOD = "Nudelauflauf"
const FOOD_FROM_HELL = "Pilze"

export function sortByTastiness(food: string[]): string[] {
    return food.sort(compareFood)
}

function compareFood(food1: string, food2: string): number {
    if (food1 === food2) {
        return 0
    }
    if (food1 === HOLY_FOOD || food2 === FOOD_FROM_HELL) {
        return -1
    }
    if (food1 === FOOD_FROM_HELL || food2 === HOLY_FOOD) {
        return 1
    }
    return 0
}