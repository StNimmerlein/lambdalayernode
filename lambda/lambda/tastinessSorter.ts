import {sortByTastiness} from 'tastiness-sorter'

export const sort = async (event: string[]) => {
    return sortByTastiness(event)
}