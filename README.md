# AWS Lambda Layer with Node

There are two subprojects:
- `layer` which creates a node library
- `lambda` which uses this library

## How to transpile ...

### ... the Layer

In `layer` run `yarn build`.

### ... the Lambda

After transpiling the layer, run `yarn install` in `lambda`.
Then run `yarn build` in `lambda`.

## How to upload ...

### ... the layer

In `layer/dist`, create a zip file that contains the directory `nodejs` and all directories/files in it. Upload this zip as a NodeJs layer zip to AWS.

### ... the lambda

In `lambda/dist`, create a zip file that contains `tastinessSorter.js`. Upload this as a NodeJs Lambda zip and set the invocation handler to `tastinessSorter.sort`.

## Note

Even though the `tastiness-sorter` library from the layer is not included in the lambda zip, it can run on AWS if it hast the correct layer attached.
If the lambda additionally relied on some other node modules, they also have to be provided via a handler or be included in  the lambda zip.

## Suggestions

If you find a more comfortable way of creating the zip files, including some node modules (but not all), copying the transpiled library to the lambda directory etc., feel free to show them to me.